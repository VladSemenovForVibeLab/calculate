# Частота встречи символов API

REST API, предоставляющее возможность вычисления частоты встречи символов в заданной строке.

## Требования

Для выполнения этого задания следует использовать следующие инструменты и технологии:

- Java 17
- Spring Boot 3.1.3
- Docker
- Docker Compose
- Swagger
- Spring Data JPA
- PostgreSQL
- Spring Security
- JWT

## Установка

1. Клонируйте репозиторий из Gitlab:

```
git clone https://gitlab.com/VladSemenovForVibeLab/calculate.git
```

2. Установите Docker и Docker Compose на вашей машине.

3. В корневом каталоге проекта выполните команду для сборки контейнера Docker:

```
docker-compose build
```

4. Запустите контейнер с помощью команды:

```
docker-compose up
```

API будет запущен на `http://localhost:8080`.

## Формат входящих/исходящих параметров

### Запросы API

API предоставляет следующие точки входа:

- `POST /frequency`: Рассчитывает частоту встречи символов в заданной строке.

Запросы для рассчета частоты встречи символов должны содержать следующую JSON-структуру:

```json
{
  "inputString": "aaaaabcccc"
}
```

### Ответы API

API возвращает следующий JSON-ответ:

```json
{
  "result": [
    {
      "symbol": "a",
      "frequency": 5
    },
    {
      "symbol": "c",
      "frequency": 4
    },
    {
      "symbol": "b",
      "frequency": 1
    }
  ]
}
```

## Аутентификация

Также реализована функция аутентификация с помощью токена JWT. Чтобы получить токен, необходимо выполнить POST-запрос на `http://localhost:8080/api/v1/login` со следующим JSON-телом:

```json
{
  "username": "your-username",
  "password": "your-password"
}
```

Ответ будет содержать токен JWT, который должен быть добавлен в заголовок Authorization для каждого запроса API:

```
Authorization: Bearer your-token
```

## Документация API

Документация API, сгенерированная Swagger, доступна по адресу `http://localhost:8080/swagger-ui.html`.


## Размещение кода

https://gitlab.com/VladSemenovForVibeLab/calculate.git

## Автор

Семенов Владислав Вячеславович.