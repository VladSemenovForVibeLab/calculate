package com.semenov.characterFrequency.controller;

import com.semenov.characterFrequency.service.CharacterFrequencyService;
import com.semenov.characterFrequency.web.controller.CharacterFrequencyController;
import com.semenov.characterFrequency.web.dto.character.CharacterFrequencyRequest;
import com.semenov.characterFrequency.web.dto.character.CharacterFrequencyResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class CharacterFrequencyControllerTest {
    @Mock
    private CharacterFrequencyService characterFrequencyService;

    @InjectMocks
    private CharacterFrequencyController characterFrequencyController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculateCharacterFrequency() {
        CharacterFrequencyRequest request = new CharacterFrequencyRequest();
        request.setInputString("hello");

        Map<Character, Integer> frequencyMap = new HashMap<>();
        frequencyMap.put('h', 1);
        frequencyMap.put('e', 1);
        frequencyMap.put('l', 2);
        frequencyMap.put('o', 1);

        when(characterFrequencyService.calculateFrequency(request.getInputString()))
                .thenReturn(frequencyMap);

        CharacterFrequencyResponse expectedResponse = new CharacterFrequencyResponse();
        expectedResponse.setFrequencyMap(frequencyMap);
        ResponseEntity<CharacterFrequencyResponse> responseEntity = characterFrequencyController.calculateCharacterFrequency(request);
        CharacterFrequencyResponse response = responseEntity.getBody();

        assertEquals(expectedResponse, response);
    }
}
