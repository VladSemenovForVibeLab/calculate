package com.semenov.characterFrequency.service.impl;

import com.semenov.characterFrequency.domain.exception.ResourceNotFoundException;
import com.semenov.characterFrequency.repository.FrequencyRepository;
import com.semenov.characterFrequency.service.CharacterFrequencyService;
import jdk.jfr.Frequency;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CharacterFrequencyServiceImplTest {
    private CharacterFrequencyService characterFrequencyService;

    @Mock
    private FrequencyRepository frequencyRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        characterFrequencyService = new CharacterFrequencyServiceImpl(frequencyRepository);
    }

    @Test
    void calculateFrequency_WithValidInputString_ShouldReturnFrequencyMap() {
        String inputString = "hello";
        Map<Character, Integer> expectedFrequencyMap = new HashMap<>();
        expectedFrequencyMap.put('l', 2);
        expectedFrequencyMap.put('o', 1);
        expectedFrequencyMap.put('h', 1);
        expectedFrequencyMap.put('e', 1);

        Map<Character, Integer> actualFrequencyMap = characterFrequencyService.calculateFrequency(inputString);

        assertEquals(expectedFrequencyMap, actualFrequencyMap);
    }

    @Test
    void calculateFrequency_WithNullInputString_ShouldThrowException() {
        String inputString = null;

        assertThrows(ResourceNotFoundException.class, () -> {
            characterFrequencyService.calculateFrequency(inputString);
        });
    }

    @Test
    void calculateFrequency_WithEmptyInputString_ShouldThrowException() {
        String inputString = "";

        assertThrows(ResourceNotFoundException.class, () -> {
            characterFrequencyService.calculateFrequency(inputString);
        });
    }

    @Test
    void sortByFrequencyDescending_WithFrequencyMap_ShouldReturnSortedFrequencyMap() {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        frequencyMap.put('l', 2);
        frequencyMap.put('o', 1);
        frequencyMap.put('h', 1);
        frequencyMap.put('e', 1);

        Map<Character, Integer> expectedSortedFrequencyMap = new LinkedHashMap<>();
        expectedSortedFrequencyMap.put('l', 2);
        expectedSortedFrequencyMap.put('e', 1);
        expectedSortedFrequencyMap.put('o', 1);
        expectedSortedFrequencyMap.put('h', 1);

        Map<Character, Integer> actualSortedFrequencyMap = characterFrequencyService.sortByFrequencyDescending(frequencyMap);

        assertEquals(expectedSortedFrequencyMap, actualSortedFrequencyMap);
    }
}

