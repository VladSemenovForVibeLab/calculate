package com.semenov.characterFrequency.domain.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

/**
 * Класс -> объект ошибки
 */
@Data
@AllArgsConstructor
public class ExceptionBody {
    private String message;
    private Map<String,String> errors;


    /**
     * Конструктор, принимающий только сообщение об ошибке
     * @param message Сообщение об ошибке
     */
    public ExceptionBody(String message) {
        this.message = message;
    }
}
