package com.semenov.characterFrequency.domain.exception;

/**
 * Класс ResourceNotFoundException является подклассом RuntimeException. Он используется для обработки исключительной
 * ситуации, когда ресурс не может быть найден.
 */
public class ResourceNotFoundException extends RuntimeException {
    /**
     * Конструктор класса ResourceNotFoundException, принимающий сообщение об ошибке.
     *
     * @param message Сообщение об ошибке
     */
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
