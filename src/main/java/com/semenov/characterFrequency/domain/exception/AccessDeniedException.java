package com.semenov.characterFrequency.domain.exception;

/**
 * Класс AccessDeniedException представляет собой пользовательское исключение, которое возникает,
 * когда у пользователя отсутствуют необходимые права доступа.
 */
public class AccessDeniedException extends RuntimeException {
    /**
     * Конструктор по умолчанию класса AccessDeniedException.
     * Вызывает конструктор суперкласса RuntimeException.
     */
    public AccessDeniedException() {
        super();
    }
}
