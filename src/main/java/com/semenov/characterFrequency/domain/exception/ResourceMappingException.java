package com.semenov.characterFrequency.domain.exception;

/**
 * Класс ResourceMappingException представляет собой пользовательское исключение, которое возникает,
 * когда происходит ошибка в отображении ресурсов.
 */
public class ResourceMappingException extends RuntimeException {

    /**
     * Конструктор класса ResourceMappingException.
     * Вызывает конструктор суперкласса RuntimeException с переданным сообщением об ошибке.
     *
     * @param message сообщение об ошибке
     */
    public ResourceMappingException(String message) {
        super(message);
    }
}
