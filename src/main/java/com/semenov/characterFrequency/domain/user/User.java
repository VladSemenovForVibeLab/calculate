package com.semenov.characterFrequency.domain.user;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * Класс User представляет собой сущность пользователя, соответствующую таблице "users" в базе данных.
 * Он используется для хранения информации о пользователях, таких как их идентификатор, имя пользователя, имя,
 * пароль и роли.
 */
@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    /**
     * Идентификатор пользователя.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Имя пользователя.
     */
    private String username;

    /**
     * Имя пользователя.
     * Соответствует столбцу "name" в таблице "users".
     */
    @Column(name = "name")
    private String name;

    /**
     * Пароль пользователя.
     */
    private String password;

    /**
     * Подтверждение пароля пользователя.
     * Поле помечено как @Transient, поэтому оно не будет сохраняться в базе данных.
     */
    @Transient
    private String passwordConfirmation;

    /**
     * Роли пользователя.
     * Соответствует отношению "многие ко многим" между пользователями и их ролями.
     * Роли сохраняются в таблице "users_roles".
     */
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_roles")
    @Enumerated(value = EnumType.STRING)
    private Set<Role> roles;
}
