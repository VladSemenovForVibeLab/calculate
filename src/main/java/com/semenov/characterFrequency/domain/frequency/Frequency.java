package com.semenov.characterFrequency.domain.frequency;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс Frequency представляет сущность, описывающую частоту появления символа или строки в тексте.
 */
@Getter
@Setter
@Entity
@Table(name = "frequency")
@NoArgsConstructor
@AllArgsConstructor
public class Frequency {

    /**
     *  Идентификатор объекта Frequency.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * Строка, которая анализируется на наличие символа(char) с заданной частотой.
     */
    @Column(name = "string")
    private String string;

    /**
     * Символ, для которого вычисляется частота.
     */
    @Column(name = "character")
    private char character;

    /**
     * Частота появления символа(char) или строки(string) в тексте.
     */
    @Column(name = "frequency")
    private int frequency;

}