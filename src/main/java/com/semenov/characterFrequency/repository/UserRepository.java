package com.semenov.characterFrequency.repository;

import com.semenov.characterFrequency.domain.user.Role;
import com.semenov.characterFrequency.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Интерфейс UserRepository является репозиторием для работы с сущностью User.
 * Он предоставляет методы для выполнения операций поиска и сохранения пользователей в базе данных.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Находит пользователя по его имени пользователя.
     *
     * @param username имя пользователя
     * @return Optional объект User, содержащий найденного пользователя, или пустой объект Optional, если пользователь не найден
     */
    Optional<User> findByUsername(String username);

    /**
     * Находит пользователя по его имени и роли.
     *
     * @param name имя пользователя
     * @param role роль пользователя
     * @return Optional объект User, содержащий найденного пользователя, или пустой объект Optional, если пользователь не найден
     */
    Optional<User> findByNameAndRolesContaining(String name, Role role);
}
