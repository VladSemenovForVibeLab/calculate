package com.semenov.characterFrequency.repository;

import com.semenov.characterFrequency.domain.frequency.Frequency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс FrequencyRepository является репозиторием для работы с сущностью Frequency в базе данных.
 */
@Repository
public interface FrequencyRepository extends JpaRepository<Frequency, Long> {
    /**
     * Метод findByStringOrderByFrequencyDesc выполняет поиск объектов Frequency с заданной строкой и сортирует их по
     * частоте появления в убывающем порядке.
     *
     * @param inputString - входная строка, по которой выполняется поиск объектов Frequency.
     * @return список объектов Frequency, отсортированных по частоте появления.
     */
    List<Frequency> findByStringOrderByFrequencyDesc(String inputString);
}