package com.semenov.characterFrequency.security;

import com.semenov.characterFrequency.domain.exception.AccessDeniedException;
import com.semenov.characterFrequency.domain.user.Role;
import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.service.UserService;
import com.semenov.characterFrequency.service.props.JwtProperties;
import com.semenov.characterFrequency.web.dto.auth.JWTResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс JwtTokenProvider предоставляет методы для создания и валидации JWT-токенов.
 */
@Service
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final JwtProperties jwtProperties;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private Key key;

    /**
     * Инициализация ключа для подписи токенов.
     */
    @PostConstruct
    public void init() {
        this.key = Keys.hmacShaKeyFor(jwtProperties.getSecret().getBytes());
    }

    /**
     * Создание access токена.
     *
     * @param userId   ID пользователя.
     * @param username Имя пользователя.
     * @param roles    Роли пользователя.
     * @return access токен.
     */
    public String createAccessToken(Long userId, String username, Set<Role> roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("id", userId);
        claims.put("roles", resolveRoles(roles));
        Instant validity = Instant.now()
                .plus(jwtProperties.getAccess(), ChronoUnit.HOURS);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(validity))
                .signWith(key)
                .compact();
    }

    /**
     * Преобразование ролей пользователя в список их названий.
     *
     * @param roles Роли пользователя.
     * @return Список названий ролей.
     */
    private List<String> resolveRoles(Set<Role> roles) {
        return roles.stream()
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    /**
     * Создание refresh токена.
     *
     * @param userId   ID пользователя.
     * @param username Имя пользователя.
     * @return refresh токен.
     */
    public String createRefreshToken(Long userId, String username) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("id", userId);
        Instant validity = Instant.now()
                .plus(jwtProperties.getRefresh(), ChronoUnit.DAYS);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(validity))
                .signWith(key)
                .compact();
    }

    /**
     * Обновление access и refresh токенов пользователя.
     *
     * @param refreshToken refresh токен пользователя.
     * @return Объект JWTResponse с обновленными токенами.
     */
    public JWTResponse refreshUserTokens(String refreshToken) {
        JWTResponse jwtResponse = new JWTResponse();
        if (!validateToken(refreshToken)) {
            throw new AccessDeniedException();
        }
        Long userId = Long.valueOf(getId(refreshToken));
        User user = userService.getById(userId);
        jwtResponse.setId(userId);
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(createAccessToken(userId, user.getUsername(), user.getRoles()));
        jwtResponse.setRefreshToken(createRefreshToken(userId, user.getUsername()));
        return jwtResponse;
    }

    /**
     * Получение ID пользователя из refresh токена.
     *
     * @param token refresh токен пользователя.
     * @return ID пользователя.
     */
    private String getId(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .get("id")
                .toString();
    }

    /**
     * Валидация токена.
     *
     * @param token Токен для валидации.
     * @return true, если токен валидный, иначе false.
     */
    public boolean validateToken(String token) {
        Jws<Claims> claims = Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
        return !claims.getBody().getExpiration().before(new Date());
    }

    /**
     * Получение аутентификации пользователя по токену.
     *
     * @param token Access токен пользователя.
     * @return Аутентификация пользователя.
     */
    public Authentication getAuthentication(String token) {
        String username = getUsername(token);
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    /**
     * Получение имени пользователя из токена.
     *
     * @param token Токен.
     * @return Имя пользователя.
     */
    private String getUsername(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }
}
