package com.semenov.characterFrequency.security;

import com.semenov.characterFrequency.domain.user.Role;
import com.semenov.characterFrequency.domain.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс JwtEntityFactory предоставляет фабричные методы для создания объекта JwtEntity на основе сущности User.
 * Он используется для преобразования данных пользователя в объект JwtEntity, который содержит информацию,
 * необходимую для создания JWT токена.
 */
public class JwtEntityFactory {

    /**
     * Создает объект JwtEntity на основе сущности User.
     *
     * @param user объект User, на основе которого будет создан JwtEntity
     * @return объект JwtEntity, созданный на основе данных User
     */
    public static JwtEntity create(User user) {
        return new JwtEntity(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getPassword(),
                mapToGrantedAuthorities(new ArrayList<>(user.getRoles()))
        );
    }

    /**
     * Преобразует список ролей пользователя к списку объектов GrantedAuthority.
     *
     * @param roles список ролей пользователя
     * @return список объектов GrantedAuthority
     */
    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> roles) {
        return roles.stream()
                .map(Enum::name)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
