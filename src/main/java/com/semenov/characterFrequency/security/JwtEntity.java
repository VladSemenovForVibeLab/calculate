package com.semenov.characterFrequency.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Класс JwtEntity представляет собой информацию о пользователе, хранящуюся в JWT токене.
 * Он реализует интерфейс UserDetails, который предоставляет информацию о пользователе для аутентификации и авторизации.
 */
@Data
@AllArgsConstructor
public class JwtEntity implements UserDetails {
    /**
     * Идентификатор пользователя.
     */
    private Long id;
    /**
     * Имя пользователя.
     */
    private final String username;
    /**
     * Имя пользователя.
     */
    private final String name;
    /**
     * Пароль пользователя.
     */
    private final String password;
    /**
     * Коллекция ролей, присвоенных пользователю.
     */
    private final Collection<? extends GrantedAuthority> authorities;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    /**
     * Проверяет, истек ли срок действия учетной записи пользователя.
     * @return возвращает true, если срок действия учетной записи не истек, в противном случае - false
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Проверяет, заблокирована ли учетная запись пользователя.
     * @return возвращает true, если учетная запись пользователя не заблокирована, в противном случае - false
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Проверяет, истек ли срок действия учетных данных пользователя.
     * @return возвращает true, если срок действия учетных данных пользователя не истек, в противном случае - false
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Проверяет, активирована ли учетная запись пользователя.
     * @return возвращает true, если учетная запись пользователя активирована, в противном случае - false
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
