package com.semenov.characterFrequency.security;

import com.semenov.characterFrequency.domain.exception.ResourceNotFoundException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

/**
 * Класс JwtTokenFilter представляет собой фильтр, который обрабатывает JWT токен.
 * Он используется для проверки и аутентификации токена при выполнении запроса.
 */
@AllArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * Выполняет фильтрацию запроса.
     *
     * @param servletRequest  входящий HTTP-сервлет-запрос
     * @param servletResponse исходящий HTTP-сервлет-ответ
     * @param filterChain     цепочка фильтров, через которую должен проходить запрос
     * @throws IOException      если произошла ошибка ввода-вывода при фильтрации запроса
     * @throws ServletException если произошла сервлет-ошибка при фильтрации запроса
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // Получить токен из заголовка Authorization
        String bearerToken = ((HttpServletRequest) servletRequest).getHeader("Authorization");
        // Проверить и валидировать токен
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            bearerToken = bearerToken.substring(7);
        }
        if (bearerToken != null && jwtTokenProvider.validateToken(bearerToken)) {
            try {
                // Получить аутентификацию из токена
                Authentication authentication = jwtTokenProvider.getAuthentication(bearerToken);
                if (authentication != null) {
                   // Установить аутентификацию в контекст безопасности
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            } catch (ResourceNotFoundException ignored) {

            }
        }
        // Пропустить запрос через цепочку фильтров
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
