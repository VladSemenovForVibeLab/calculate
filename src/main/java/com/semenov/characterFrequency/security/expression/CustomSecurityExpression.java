package com.semenov.characterFrequency.security.expression;

import com.semenov.characterFrequency.domain.user.Role;
import com.semenov.characterFrequency.security.JwtEntity;
import com.semenov.characterFrequency.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Класс CustomSecurityExpression представляет собой сервис с пользовательскими выражениями безопасности.
 * Он используется для проверки доступа пользователя к определенным операциям.
 */
@Service("customSecurityExpression")
@RequiredArgsConstructor
public class CustomSecurityExpression {
    private final UserService userService;

    /**
     * Проверяет, может ли пользователь получить доступ к определенному пользователю.
     *
     * @param id идентификатор пользователя, к которому требуется получить доступ
     * @return true, если пользователь может получить доступ, в противном случае - false
     */
    public boolean canAccessUser(Long id) {
        // Получить аутентификацию пользователя из контекста безопасности
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Получить данного пользователя из аутентификации
        JwtEntity user = (JwtEntity) authentication.getPrincipal();
        Long userId = user.getId();
        // Проверить, является ли пользователь владельцем или администратором
        return userId.equals(id) || hasAnyRole(authentication, Role.ROLE_ADMIN);
    }

    /**
     * Проверяет, есть ли у пользователя хотя бы одна из указанных ролей.
     *
     * @param authentication аутентификация пользователя
     * @param roles          массив ролей, которые требуется проверить
     * @return true, если у пользователя есть хотя бы одна из указанных ролей, в противном случае - false
     */
    private boolean hasAnyRole(Authentication authentication, Role... roles) {
        for (Role role : roles) {
            // Создайте объект SimpleGrantedAuthority с именем соответствующей роли
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
            // Проверить, содержит ли аутентификация пользователя указанную роль
            if (authentication.getAuthorities().contains(authority)) {
                return true;
            }
        }
        return false;
    }
}
