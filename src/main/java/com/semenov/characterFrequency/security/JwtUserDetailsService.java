package com.semenov.characterFrequency.security;

import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Класс JwtUserDetailsService предоставляет реализацию интерфейса UserDetailsService для аутентификации пользователей по JWT-токену.
 */
@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserService userService;

    /**
     * Загружает пользователя по его имени пользователя.
     *
     * @param username Имя пользователя.
     * @return Данные пользователя для аутентификации.
     * @throws UsernameNotFoundException Если пользователь с указанным именем не найден.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByUsername(username);
        return JwtEntityFactory.create(user);
    }
}
