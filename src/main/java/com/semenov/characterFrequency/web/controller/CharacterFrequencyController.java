package com.semenov.characterFrequency.web.controller;

import com.semenov.characterFrequency.service.CharacterFrequencyService;
import com.semenov.characterFrequency.web.dto.character.CharacterFrequencyRequest;
import com.semenov.characterFrequency.web.dto.character.CharacterFrequencyResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Контроллер для подсчета частоты символов в строке
 */
@RestController
@RequestMapping("/api/v1/calculate")
@RequiredArgsConstructor
@Validated
@Tag(name = "Controller Counter",description = "Controller for counting the frequency of words in a string")
public class CharacterFrequencyController {
    private final CharacterFrequencyService characterFrequencyService;

    /**
     * Метод для подсчета частоты символов в строке
     *
     * @param request - объект запроса, содержащий входную строку
     * @return объект ответа, содержащий карту частоты символов
     */
    @PostMapping("/frequency")
    @Operation(summary = "Counting the frequency of letters in a string")
    public ResponseEntity<CharacterFrequencyResponse> calculateCharacterFrequency(@Validated @RequestBody CharacterFrequencyRequest request){
        Map<Character,Integer> frequencyMap = characterFrequencyService.calculateFrequency(request.getInputString());
        CharacterFrequencyResponse response = new CharacterFrequencyResponse();
        response.setFrequencyMap(frequencyMap);
        return ResponseEntity.ok(response);
    }
}
