package com.semenov.characterFrequency.web.controller;

import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.service.AuthService;
import com.semenov.characterFrequency.service.UserService;
import com.semenov.characterFrequency.web.dto.auth.JWTRequest;
import com.semenov.characterFrequency.web.dto.auth.JWTResponse;
import com.semenov.characterFrequency.web.dto.user.UserDto;
import com.semenov.characterFrequency.web.dto.validation.OnCreate;
import com.semenov.characterFrequency.web.mappers.UserMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Validated
@Tag(name = "Authentication Controller",description = "Authentication API")
public class AuthController {
    private final AuthService authService;
    private final UserService userService;
    private final UserMapper userMapper;

    /**
     * Метод для выполнения входа пользователя.
     *
     * @param loginRequest объект, содержащий данные для входа пользователя
     * @return объект JWTResponse с токенами аутентификации
     */
    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }


    /**
     * Метод для регистрации нового пользователя.
     *
     * @param userDto объект, содержащий данные нового пользователя
     * @return объект UserDto с данными зарегистрированного пользователя
     */
    @PostMapping("/register")
    public UserDto register(@Validated(OnCreate.class) @RequestBody UserDto userDto){
        User user = userMapper.toEntity(userDto);
        User createdUser = userService.create(user);
        return userMapper.toDto(createdUser);
    }

    /**
     * Метод для обновления токена аутентификации.
     *
     * @param refreshToken строка содержащая Refresh Token
     * @return объект JWTResponse с обновленными токенами аутентификации
     */
    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }

}
