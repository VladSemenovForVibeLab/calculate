package com.semenov.characterFrequency.web.controller;

import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.service.UserService;
import com.semenov.characterFrequency.web.dto.user.UserDto;
import com.semenov.characterFrequency.web.dto.validation.OnCreate;
import com.semenov.characterFrequency.web.dto.validation.OnUpdate;
import com.semenov.characterFrequency.web.mappers.UserMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.config.Task;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Validated
@Tag(name = "User Controller", description = "User API")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    /**
     * Обновление пользователя
     *
     * @param dto - объект DTO пользователя, содержащий обновленные данные
     * @return объект DTO обновленного пользователя
     */
    @PutMapping
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Update User")
    public UserDto update(@Validated(OnUpdate.class) @RequestBody UserDto dto) {
        User user = userMapper.toEntity(dto);
        User updatedUser = userService.update(user);
        return userMapper.toDto(updatedUser);
    }

    /**
     * Получение пользователя по идентификатору
     *
     * @param id - идентификатор пользователя
     * @return объект DTO пользователя
     */
    @GetMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#id)")
    @Operation(summary = "Get UserDto by Id")
    public UserDto getById(@PathVariable Long id) {
        User user = userService.getById(id);
        return userMapper.toDto(user);
    }

    /**
     * Удаление пользователя по идентификатору
     *
     * @param id - идентификатор пользователя
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Delete User by Id")
    public void deleteById(@PathVariable Long id) {
        userService.delete(id);
    }

}
