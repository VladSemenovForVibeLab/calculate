package com.semenov.characterFrequency.web.controller;

import com.semenov.characterFrequency.domain.exception.AccessDeniedException;
import com.semenov.characterFrequency.domain.exception.ExceptionBody;
import com.semenov.characterFrequency.domain.exception.ResourceNotFoundException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdvice {

    /**
     * Обработчик исключения ResourceNotFoundException
     *
     * @param e - объект исключения ResourceNotFoundException
     * @return объект ExceptionBody с сообщением об ошибке
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionBody handleResourceNotFound(ResourceNotFoundException e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения IllegalStateException
     *
     * @param e - объект исключения IllegalStateException
     * @return объект ExceptionBody с сообщением об ошибке
     */
    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения MethodArgumentNotValidException
     *
     * @param e - объект исключения MethodArgumentNotValidException
     * @return объект ExceptionBody с сообщением об ошибке и списком ошибок валидации
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        ExceptionBody exceptionBody = new ExceptionBody("Validation failed");
        List<FieldError> errors = e.getBindingResult().getFieldErrors();
        exceptionBody.setErrors(errors.stream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)));
        return exceptionBody;
    }

    /**
     * Обработчик исключения AuthenticationException
     *
     * @param e - объект исключения AuthenticationException
     * @return объект ExceptionBody с сообщением об ошибке
     */
    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleAuthentication(AuthenticationException e){
        return new ExceptionBody("Authentication failed");
    }

    /**
     * Обработчик исключения, не относящегося к другим обработчикам
     *
     * @param e - объект исключения
     * @return объект ExceptionBody с сообщением об ошибке
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionBody handleException(Exception e) {
        return new ExceptionBody("Internal error");
    }

    /**
     * Обработчик исключения ConstraintViolationException
     *
     * @param e - объект исключения ConstraintViolationException
     * @return объект ExceptionBody с сообщением об ошибке и списком ошибок валидации
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleConstraintViolation(ConstraintViolationException e) {
        ExceptionBody exceptionBody = new ExceptionBody("Validation failed");
        exceptionBody.setErrors(e.getConstraintViolations().stream()
                .collect(Collectors.toMap(
                        violation -> violation.getPropertyPath().toString(),
                        violation -> violation.getMessage()
                )));
        return exceptionBody;
    }

    /**
     * Обработчик исключения AccessDeniedException
     *
     * @return объект ExceptionBody с сообщением об отказе в доступе
     */
    @ExceptionHandler({AccessDeniedException.class, org.springframework.security.access.AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ExceptionBody handleAccessDenied() {
        return new ExceptionBody("Access denied. ");
    }

}
