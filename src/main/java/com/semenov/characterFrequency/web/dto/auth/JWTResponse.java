package com.semenov.characterFrequency.web.dto.auth;

import lombok.Data;

@Data
public class JWTResponse {
    /**
     * Идентификатор пользователя.
     */
    private Long id;
    /**
     * Имя пользователя.
     */
    private String username;
    /**
     * Токен доступа.
     */
    private String accessToken;
    /**
     * Токен обновления.
     */
    private String refreshToken;
}
