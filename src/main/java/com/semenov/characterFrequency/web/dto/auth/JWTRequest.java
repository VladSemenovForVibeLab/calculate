package com.semenov.characterFrequency.web.dto.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * Класс, представляющий запрос на вход пользователя.
 */
@Data
@Schema(description = "Request for login")
public class JWTRequest {
    /**
     * Электронная почта пользователя.
     */
    @Schema(description = "email",example = "vlad@gmail.com")
    @NotNull(message = "Username must be not null")
    private String username;
    /**
     * Пароль пользователя.
     */
    @Schema(description = "password for request",example = "123")
    @NotNull(message = "Password must be not null")
    private String password;
}
