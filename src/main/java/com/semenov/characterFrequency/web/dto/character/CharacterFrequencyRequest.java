package com.semenov.characterFrequency.web.dto.character;

import com.semenov.characterFrequency.web.dto.validation.OnCreate;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * Класс, представляющий запрос на подсчет частоты символов.
 */
@Data
@Schema(description = "CharacterFrequencyRequest")
public class CharacterFrequencyRequest {
    /**
     * Входная строка для подсчета частоты символов.
     */
    @NotNull(message = "Input stream must be not null",groups = OnCreate.class)
    @Schema(description = "String",example = "aaaaabcccc")
    private String inputString;
}
