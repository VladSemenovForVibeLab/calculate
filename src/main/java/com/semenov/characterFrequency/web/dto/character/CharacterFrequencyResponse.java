package com.semenov.characterFrequency.web.dto.character;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.Map;

/**
 * Класс CharacterFrequencyResponse представляет собой модель ответа, содержащего частоту символов.
 * Он содержит одно поле frequencyMap, которое представляет собой карту символов и их частоты.
 */
@Data
@Schema(description = "CharacterFrequencyResponse")
public class CharacterFrequencyResponse {
    /**
     * Выходная мапа с символом и его частотой.
     */
    @NotNull(message = "Response must be not null")
    @Schema(description = "Map", example = "“a”: 5, “c”: 4, “b”: 1")
    private Map<Character, Integer> frequencyMap;
}
