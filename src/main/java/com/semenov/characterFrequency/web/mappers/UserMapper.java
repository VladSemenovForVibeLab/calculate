package com.semenov.characterFrequency.web.mappers;

import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.web.dto.user.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends Mappable<User, UserDto> {
}