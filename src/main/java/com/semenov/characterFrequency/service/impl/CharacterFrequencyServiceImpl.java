package com.semenov.characterFrequency.service.impl;

import com.semenov.characterFrequency.domain.exception.ResourceNotFoundException;
import com.semenov.characterFrequency.domain.frequency.Frequency;
import com.semenov.characterFrequency.repository.FrequencyRepository;
import com.semenov.characterFrequency.service.CharacterFrequencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс CharacterFrequencyServiceImpl реализует интерфейс CharacterFrequencyService и предоставляет реализацию методов
 * для вычисления частоты встречи символов в заданной строке и сортировки результирующей мапы по убыванию частоты.
 */
@Service
@RequiredArgsConstructor
public class CharacterFrequencyServiceImpl implements CharacterFrequencyService {

    private final FrequencyRepository frequencyRepository;

    /**
     * Вычисляет частоту встречи символов в заданной строке.
     *
     * @param inputString Входная строка, для которой нужно вычислить частоту встречи символов
     * @return Мапа, содержащая символы и их частоту встречи в убывающем порядке
     */
    @Override
    public Map<Character, Integer> calculateFrequency(String inputString) {
        if (inputString == null || inputString.isEmpty()) {
            throw new ResourceNotFoundException("Вы ничего не ввели для подсчета");
        }
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (char c : inputString.toCharArray()) {
            frequencyMap.put(c, frequencyMap.getOrDefault(c, 0) + 1);
        }
        saveCalculateFrequency(inputString, frequencyMap.entrySet());
        return sortByFrequencyDescending(frequencyMap);
    }

    /**
     * Сохраняет частоту символов во входной строке.
     *
     * @param inputString Входная строка.
     * @param entries     Множество записей, содержащих символы и их частоты.
     */
    private void saveCalculateFrequency(String inputString, Set<Map.Entry<Character, Integer>> entries) {
        for (Map.Entry<Character, Integer> entry : entries) {
            Frequency frequency = new Frequency();
            frequency.setString(inputString);
            frequency.setCharacter(entry.getKey());
            frequency.setFrequency(entry.getValue());
            frequencyRepository.save(frequency);
        }
    }

    /**
     * Сортирует мапу частоты встречи символов в убывающем порядке.
     *
     * @param frequencyMap Мапа с символами и их частотой встречи
     * @return Отсортированная мапа в убывающем порядке
     */
    @Override
    public Map<Character, Integer> sortByFrequencyDescending(Map<Character, Integer> frequencyMap) {
        Map<Character, Integer> sortedFrequencyMap = frequencyMap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(
                        Map.Entry.comparingByValue()
                ))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Character, Integer> entry : sortedFrequencyMap.entrySet()) {
            result.append(entry.getKey()).append(":").append(entry.getValue()).append(", ");
        }

        if (result.length() > 0) {
            result.delete(result.length() - 2, result.length());
        }

        System.out.println(result.toString());
        return sortedFrequencyMap;
    }

}
