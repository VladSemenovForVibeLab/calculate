package com.semenov.characterFrequency.service.impl;

import com.semenov.characterFrequency.domain.exception.ResourceNotFoundException;
import com.semenov.characterFrequency.domain.user.Role;
import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.repository.UserRepository;
import com.semenov.characterFrequency.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Реализация сервиса пользователя.
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * Получает пользователя по заданному идентификатору.
     *
     * @param id Идентификатор пользователя.
     * @return Пользователь с заданным идентификатором.
     * @throws ResourceNotFoundException Если пользователь не найден.
     */
    @Override
    @Transactional(readOnly = true)
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    /**
     * Получает пользователя по заданному имени пользователя.
     *
     * @param username Имя пользователя.
     * @return Пользователь с заданным именем пользователя.
     * @throws ResourceNotFoundException Если пользователь не найден.
     */
    @Override
    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    /**
     * * Обновляет информацию о пользователе.
     * * @param user Обновленные данные пользователя.
     * * @return Обновленный пользователь.
     */
    @Override
    @Transactional
    public User update(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    /**
     * Создает нового пользователя.
     * @param user Данные нового пользователя.
     * @return Созданный пользователь.
     */
    @Override
    @Transactional
    public User create(User user) {
        if (userRepository.findByUsername(user.getUsername()).isPresent()) {
            throw new IllegalStateException("User already exists.");
        }
        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            throw new IllegalStateException("Password and password confirmation do not match");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Role> roles = Set.of(Role.ROLE_USER);
        user.setRoles(roles);
        userRepository.save(user);
        return user;
    }

    /**
     * Удаляет пользователя по заданному идентификатору.
     * @param id Идентификатор пользователя.
     */
    @Override
    @Transactional
    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
