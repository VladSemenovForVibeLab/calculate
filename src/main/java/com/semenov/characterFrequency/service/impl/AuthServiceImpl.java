package com.semenov.characterFrequency.service.impl;

import com.semenov.characterFrequency.domain.user.User;
import com.semenov.characterFrequency.security.JwtTokenProvider;
import com.semenov.characterFrequency.service.AuthService;
import com.semenov.characterFrequency.service.UserService;
import com.semenov.characterFrequency.web.dto.auth.JWTRequest;
import com.semenov.characterFrequency.web.dto.auth.JWTResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

/**
 * Реализация интерфейса AuthService.
 * Осуществляет аутентификацию пользователя и обновление токенов доступа.
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;
    /**
     * Метод для выполнения входа пользователя.
     *
     * @param loginRequest запрос на вход пользователя
     * @return ответ с токеном доступа и обновления
     */
    @Override
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        // Аутентификация пользователя
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
        // Получение информации о пользователе
        User user = userService.getByUsername(loginRequest.getUsername());
        // Установка полей ответа
        jwtResponse.setId(user.getId());
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getId(),user.getUsername(),user.getRoles()));
        jwtResponse.setRefreshToken(jwtTokenProvider.createRefreshToken(user.getId(),user.getUsername()));
        return jwtResponse;
    }

    /**
     * Метод для обновления токена доступа.
     *
     * @param refreshToken токен обновления
     * @return ответ с новым токеном доступа и обновления
     */
    @Override
    public JWTResponse refresh(String refreshToken) {
        return jwtTokenProvider.refreshUserTokens(refreshToken);
    }
}
