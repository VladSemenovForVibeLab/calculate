package com.semenov.characterFrequency.service.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Класс, содержащий настройки для генерации и валидации JWT токенов.
 */
@Component
@Data
@ConfigurationProperties(prefix = "security.jwt")
public class JwtProperties {
    /**
     * Секретный ключ для генерации и валидации JWT токенов.
     */
    private String secret;

    /**
     * Время в часах, через которое истекает токен доступа.
     */
    private long access;
    /**
     * Время в днях, через которое истекает токен обновления.
     */
    private long refresh;
}
