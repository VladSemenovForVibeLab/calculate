package com.semenov.characterFrequency.service;

import java.util.Map;

/**
 * Интерфейс сервиса для подсчета частоты символов в строке.
 */
public interface CharacterFrequencyService {

    /**
     * Метод для подсчета частоты символов в строке.
     *
     * @param inputString входная строка
     * @return отображение, где ключ - символ, а значение - количество его вхождений в строку
     */
    Map<Character, Integer> calculateFrequency(String inputString);

    /**
     * Метод для сортировки отображения с частотой символов в порядке убывания частоты.
     *
     * @param frequencyMap отображение, где ключ - символ, а значение - количество его вхождений в строку
     * @return отсортированное отображение, где ключ - символ, а значение - количество его вхождений в строку
     */
    Map<Character, Integer> sortByFrequencyDescending(Map<Character, Integer> frequencyMap);

}
