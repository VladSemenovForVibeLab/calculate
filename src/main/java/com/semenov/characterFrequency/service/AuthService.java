package com.semenov.characterFrequency.service;


import com.semenov.characterFrequency.web.dto.auth.JWTRequest;
import com.semenov.characterFrequency.web.dto.auth.JWTResponse;

/**
 * Интерфейс для аутентификации пользователя.
 */
public interface AuthService {
    /**
     * Метод для выполнения входа пользователя.
     *
     * @param loginRequest запрос на вход пользователя
     * @return ответ с токеном доступа и обновления
     */
    JWTResponse login(JWTRequest loginRequest);

    /**
     * Метод для обновления токена доступа.
     *
     * @param refreshToken токен обновления
     * @return ответ с новым токеном доступа и обновления
     */
    JWTResponse refresh(String refreshToken);
}
