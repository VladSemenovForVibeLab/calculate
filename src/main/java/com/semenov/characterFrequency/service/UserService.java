package com.semenov.characterFrequency.service;


import com.semenov.characterFrequency.domain.user.User;

/**
 * Интерфейс сервиса для работы с пользователями.
 */
public interface UserService {

    /**
     * Метод для получения пользователя по его идентификатору.
     *
     * @param id идентификатор пользователя
     * @return объект пользователя
     */
    User getById(Long id);

    /**
     * Метод для получения пользователя по его имени пользователя (логину).
     *
     * @param username имя пользователя
     * @return объект пользователя
     */
    User getByUsername(String username);

    /**
     * Метод для обновления информации о пользователе.
     *
     * @param user объект пользователя с обновленными данными
     * @return обновленный объект пользователя
     */
    User update(User user);

    /**
     * Метод для создания нового пользователя.
     *
     * @param user объект нового пользователя
     * @return созданный объект пользователя
     */
    User create(User user);

    /**
     * Метод для удаления пользователя по его идентификатору.
     *
     * @param id идентификатор пользователя
     */
    void delete(Long id);
}
