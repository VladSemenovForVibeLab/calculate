package com.semenov.characterFrequency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class CharacterFrequencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacterFrequencyApplication.class, args);
	}

}
